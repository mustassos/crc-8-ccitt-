----------------------------------------------------------------------------------
-- Company: Brno University of Technology
-- Students : Anastasios Eleftheriadis and Mustafa Çevik

-- Module Name:    crc8 - Behavioral 
-- Project Name:   CRC calculation    
-- Target Devices: Coolrunner II
-- Description:  An example of CRC calculation base on polynomial            
--                  / x^8 + x^2 + x + 1 and init value 0x0000			
--					(which corresponds with the CCITT method)             	
--				using the expansion board as 8-bit input. The design is based    
--                  on XOR logical doors and serial registers. 
--
--
-- Additional Comments: We inspired and get help of the work done by Alejandro Lopez Palma
--                      https://gitlab.com/lopez37dev/CRC_CCITT_XMODEM
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

entity crc8 is
 generic(
        NBITS:   integer := 16 -- Number of bits of the binary number
    );
	
 port (
      SW_EXP	:in std_logic_vector(NBITS-1 downto 0); 			-- Input switches (Binary)
	  CLK    :in std_logic;                          			-- Clock
	  LED_EXP :out std_logic_vector(NBITS-1 downto 0); 			-- Output, LEDs (Binary)
      BTN0    :in std_logic;                         		   -- Button 0 working as RST, low active
      D_POS	:out std_logic_vector(3 downto 0); 				       -- Display positions
      disp_segs:out std_logic_vector(6 downto 0) :=(others => '0') -- Display segments
    );
end crc8;

architecture Behavioral of crc8 is


  signal lfsr: std_logic_vector (NBITS-1 downto 0);				  -- Set of registers to generate CRC code
  signal crc_out: std_logic_vector (NBITS-1 downto 0);		   	  -- CRC-8 code calculated
  signal clk_10	:std_logic := '0';                                -- Clock 0.1 ms signal 
  signal position :std_logic_vector(3 downto 0):=(others => '0'); -- 7-seg displays matrix 
  signal display_number	:std_logic_vector(3 downto 0):=(others => '0');   -- Split BCD value to represent into displays
  
   
begin
    -- CRC Calculation
    process (clk)
		variable i: integer :=NBITS;
	begin
         if (rising_edge(clk)) then
             if (BTN0 = '0') then
                 lfsr <= "0000000000000000";
				 i:=NBITS;
             elsif (i /= 0) then
					 i := i-1;
                     lfsr(0)  <= SW_EXP(i) xor lfsr(7);
                     lfsr(1)  <= lfsr(0) xor lfsr (7);
                     lfsr(2)  <= lfsr(1) xor lfsr(7);
                     lfsr(3)  <= lfsr(2);
                     lfsr(4)  <= lfsr(3);
                     lfsr(5)  <= lfsr(4) ;
                     lfsr(6)  <= lfsr(5);
                     lfsr(7)  <= lfsr(6);
                    
             end if;
         end if;
     end process;
 
     crc_out <= lfsr;       -- Output of the CRC calculation (Remainder)

	 LED_EXP <= SW_EXP;       --LEDs according to binary input

	
	
   process (CLK)
    begin
        if rising_edge(CLK) then
             clk_10 <= not clk_10;
        end if;
   end process;
	---------------------------------------------------------
   process (clk_10)
    begin
        if rising_edge(clk_10) then
            if position = "0111" then
                display_number <= crc_out(3 downto 0);
                position <= "1110";
            elsif position = "1110" then
                display_number <= crc_out(7 downto 4);  
                position <= "1101";
            elsif position = "1101" then 
                display_number <= crc_out(11 downto 8); 
                position <= "1011";
            elsif position <= "1011" then
                display_number <= crc_out(15 downto 12);   
                position <= "0111";
            end if;
         end if;
    end process;

    ----------------------------------------------------------
       -- seven segment led controlling
    with display_number select                     
        disp_segs <= "1111001" when "0001",     -- 1      
                 "0100100" when "0010",     -- 2
                 "0110000" when "0011",     -- 3      
                 "0011001" when "0100",     -- 4    
                 "0010010" when "0101",     -- 5       
                 "0000010" when "0110",     -- 6        
                 "1111000" when "0111",     -- 7
                 "0000000" when "1000",     -- 8    
                 "0010000" when "1001",     -- 9
				 "0001000" when "1010",		-- A
				 "0000011" when "1011",		-- b
				 "1000110" when "1100",		-- C
				 "0100001" when "1101",		-- d
				 "0000110" when "1110",		-- E
				 "0001110" when "1111",		-- F
                 "1000000" when others;     -- 0
        D_POS <= position;

    --------------------------------------------------- 


end Behavioral;

                 	            
					                                        								



